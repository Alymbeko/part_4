class Car:
    def __init__(self, make, model, year, odometer=0, fuel=70):
        self.make = make
        self.model = model
        self.year = year
        self.odometer = 0
        self.fuel = 70

    def drive(self, km):
        oil_in_fuel = self.fuel * 10
        if oil_in_fuel >= km:
            self.odometer =+ km
            spent_oil = km/10
            self.fuel -= spent_oil
            print("Let's drive")
        else:
            print('Need more fule, please, fill')
    
    def get_status(self):
        print(f' Make: {self.make}\n Model: {self.model}\n Year: {self.year}\n Odomtere: {self.odometer} km\n Fuel: {self.fuel} l')


BMW = Car('BMW', 'X6', '2017')
BMW.drive(700)
BMW.get_status()
