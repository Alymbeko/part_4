class Airplan:
    def __init__(self, make, model, year, max_speed, odometer=0, is_flying=False):
        self.make = make
        self.model = model
        self.year = year
        self.max_speed = max_speed
        self.odometer = 0
        self.is_flying = False

    def take_off(self):
        self.is_flying = True

    def fly(self, km):
        self.odometer += km
    
    def land(self):
        self.is_flying = False
    
    def get_status(self):
        print(f' Name: {self.make}\n Model: {self.model}\n Year: {self.year}\n Max speed: {self.max_speed}\n Odometer: {self.odometer}\n Status: {self.is_flying}')

Boing = Airplan('Boing', '777', '2019', '900')
Boing.take_off()
Boing.fly(5000)
Boing.land()
Boing.get_status()


    
